# shellp: a help file for POSIX systems

Does it frustrate you that there's no intuitive help system for someone totally new to the UNIX shell?
Have you ever seen a new user in front of a UNIX shell?
They stare at it blankly for a while, and then wander away.
Or maybe they get up the nerve to type something, like ``help`` at the prompt, only to get a listing of built-in commands with no context.

*This is why people say UNIX isn't user-friendly.*

## Install shellp

This is a simple, gentle introduction to the UNIX shell for new users.
If you install this, then a new user can type ``help`` at a prompt and they'll get a short course on how to use the UNIX shell.

It's that simple.

This isn't a complete course on the UNIX shell, but it's a good overview designed to give a new user practical training on what to do at an empty prompt.
It does require reading, so if a new user doesn't want to read, then this won't help them, but at least it provides useful information for anyone who barely understands what Linux or UNIX is.

*This is the user-friendly help file you've been looking for.*

## Requirements

* GNU ``info`` (the texinfo package)
* GNU autotools

